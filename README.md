# Python Steam Web API

A steam web API for python

To retrieve app details run:

    from steam_api.steam import Steam
    
    app = Steam.get_app_details("542220")

Replace _542220_ with your app id