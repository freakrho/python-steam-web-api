from typing import Optional

import requests
import json

from steam_api.structures import App

API_URL = "http://store.steampowered.com/api"


def method_url(method: str) -> str:
    return "{url}/{method}/".format(url=API_URL, method=method)


APP_DETAILS = method_url("appdetails")


class Steam:
    @staticmethod
    def send_query(url: str, params: dict) -> requests.Response:
        req = requests.get(url, params=params)
        return req

    @staticmethod
    def get_app_details(app_id: str) -> Optional[App]:
        data = json.loads(Steam.send_query(APP_DETAILS, {"appids": app_id}).text)
        if app_id in data:
            return App(data[app_id])
