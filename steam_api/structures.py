from enum import Enum, Flag
from typing import Optional


class AppName:
    def __init__(self, data: dict):
        self.appid = data["appid"]
        self.name = data["name"]


class AppDescription:
    def __init__(self, data: dict):
        self.appid = data["appid"]
        self.description = data["description"]


class Requirements:
    def __init__(self, data: dict):
        self.minimum = data["minimum"]  # String (html).
        self.recommended = data["recommended"]  # String (html).


class PriceOverview:
    def __init__(self, data: Optional[dict]):
        self.free_to_play = data is None
        if not self.free_to_play:
            self.currency = data["currency"]
            self.initial = data["initial"]
            self.final = data["final"]
            self.discount_percent = data["discount_percent"]


class Subscription:
    def __init__(self, data: dict):
        self.packageid = data["packageid"]
        self.percent_savings_text = data["percent_savings_text"]
        self.percent_savings = data["percent_savings"]
        self.option_text = data["option_text"]
        self.option_description = data["option_description"]


class AppPackage:
    def __init__(self, data: dict):
        self.name = data["name"]
        self.title = data["title"]
        self.description = data["description"]
        self.selection_text = data["selection_text"]  # If display_type is 1, this describes what the subs represent.
        self.save_text = data["save_text"]  # Marketing text about the massive savings you'll get!
        '''
        Numeric value noting how it should be displayed on store pages.
        0 - list subs as separate purchase blocks.
        1 - list subs in a drop down box, contained within a single purchase block for the package group.
        '''
        self.display_type = data["display_type"]

        self.is_recurring_subscription = data["is_recurring_subscription"]
        self.subs = []
        subs = data["subs"]  # Array of subscriptions in this package group.
        for sub in subs:
            self.subs.append(Subscription(sub))


class ScorePageData:
    def __init__(self, data: dict):
        self.score = data["score"]
        self.url = data["url"]


class Category:
    def __init__(self, data: dict):
        self.id = data["id"]
        self.description = data["description"]


class Screenshot:
    def __init__(self, data: dict):
        self.id = data["id"]
        self.path_thumbnail = data["path_thumbnail"]  # URL to image.
        self.path_full = data["path_full"]  # URL to image.


class WebM:
    def __init__(self, data: dict):
        self.url_480 = data["480"]  # URL of 480p video.
        self.url_max = data["max"]  # URL of max-quality video.


class Movie:
    def __init__(self, data: dict):
        self.id = data["id"]
        self.name = data["name"]
        self.thumbnail = data["thumbnail"]
        self.webm = WebM(data["webm"])
        self.highlight = data["highlight"]  # Boolean; not sure it's purpose.


class Achievement:
    def __init__(self, data: dict):
        self.name = data["name"]
        self.path = data["path"]  # URL to achievement icon.


class Achievements:
    def __init__(self, data: dict):
        self.total = data["total"]
        self.highlighted = []
        for achievement in data["highlighted"]:
            self.highlighted.append(Achievement(achievement))


class AppData:
    def __init__(self, data: dict):
        self.type = AppType.get(data["type"])
        self.name = data["name"]
        self.steam_appid = data["steam_appid"]
        self.required_age = int(data["required_age"])
        # optional
        if "controller_support" in data:
            self.controller_support = ControllerSupport.get(data["controller_support"])
        else:
            self.controller_support = None

        if "dlc" in data:
            self.dlc = data["dlc"]
        else:
            self.dlc = []

        self.detailed_description = data["detailed_description"]
        self.about_the_game = data["about_the_game"]
        self.short_description = data["short_description"]

        if "fullgame" in data:
            self.fullgame = AppName(data["fullgame"])
        else:
            self.fullgame = None

        self.supported_languages = data["supported_languages"]
        self.header_image = data["header_image"]  # URL to image.
        self.website = data["website"]

        # Requirements
        self.pc_requirements = Requirements(data["pc_requirements"])
        self.mac_requirements = Requirements(data["mac_requirements"])
        self.linux_requirements = Requirements(data["linux_requirements"])

        if "legal_notice" in data:
            self.legal_notice = data["legal_notice"]
        else:
            self.legal_notice = None

        if "developers" in data:
            self.developers = data["developers"]
        else:
            self.developers = []

        if "publishers" in data:
            self.publishers = data["publishers"]
        else:
            self.publishers = []

        if "demos" in data:
            self.demos = AppDescription(data["demos"])
        else:
            self.demos = None

        if "price_overview" in data:
            self.price_overview = PriceOverview(data["price_overview"])
        else:
            self.price_overview = PriceOverview(None)

        self.packages = data["packages"]
        self.package_groups = []  # Array of purchase options
        packages = data["package_groups"]
        for package in packages:
            self.package_groups.append(AppPackage(package))

        self.platforms = Platforms(0)
        platform_data = data["platforms"]
        if platform_data["windows"]:
            self.platforms |= Platforms.Windows
        if platform_data["mac"]:
            self.platforms |= Platforms.MacOS
        if platform_data["linux"]:
            self.platforms |= Platforms.Linux

        if "metacritic" in data:
            self.metacritic = ScorePageData(data["metacritic"])
        else:
            self.metacritic = None

        self.categories = []
        if "categories" in data:
            categories = data["categories"]
            for cat in categories:
                self.categories.append(Category(cat))

        self.genres = []
        if "genres" in data:
            genres = data["genres"]
            for gen in genres:
                self.genres.append(Category(gen))

        self.screenshots = []
        if "screenshots" in data:
            screenshots = data["screenshots"]
            for screenshot in screenshots:
                self.screenshots.append(Screenshot(screenshot))

        self.movies = []
        if "movies" in data:
            movies = data["movies"]
            for movie in movies:
                self.movies.append(Movie(movie))

        self.recommendations = 0
        if "recommendations" in data:
            self.recommendations = data["recommendations"]["total"]

        self.achievements = None
        if "achievements" in data:
            self.achievements = Achievements(data["achievements"])

        self.release_date = data["release_date"]["date"]  # Format is localized, according to the cc parameter passed.
        # Is an empty string when date is unannounced.

        self.coming_soon = data["release_date"]["coming_soon"]  # Boolean, true if unreleased; false if released.


class App:
    def __init__(self, data: dict):
        self.raw_data = data
        self.success = data["success"]
        self.data = AppData(data["data"])


# Enums
class Platforms(Flag):
    Windows = 0b001
    MacOS = 0b010
    Linux = 0b100


def get_enum_value(enum_type, value: str):
    for enum_value in enum_type:
        if enum_value.value == value:
            return enum_value


class AppType(Enum):
    Game = "game"
    DLC = "dlc"
    Demo = "demo"
    Advertising = "advertising"
    Mod = "mod"
    Video = "video"

    @staticmethod
    def get(value: str):
        enum_value = get_enum_value(AppType, value)
        if enum_value is not None:
            return enum_value
        return AppType.Game


class ControllerSupport(Enum):
    Partial = "partial"
    Full = "full"

    @staticmethod
    def get(value: str):
        enum_value = get_enum_value(AppType, value)
        if enum_value is not None:
            return enum_value
        return AppType.Game
